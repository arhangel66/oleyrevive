# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django_mobile.cache import cache_page

from oleyrevive.main import views
cache_time = 1

urlpatterns = \
    [
        # url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
        url(r'^$', views.HomeView.as_view(template_name='index_test.html'), name='home'),
        url(r'^test/$', views.HomeView.as_view(template_name='index.html'), name='home_test'),
        url(r'^checkout/$', views.CheckoutView.as_view(template_name='checkout.html'), name='checkout'),
        url(r'^upsell/$', views.UpsellView.as_view(template_name='upsell3.html'), name='upsell'),
        url(r'^upsell2/$', views.Upsell2View.as_view(template_name='upsell2.html'), name='upsell2'),
        url(r'^success/$', views.SuccessView.as_view(template_name='success.html'), name='success'),
        # User management
        url(settings.ADMIN_URL, admin.site.urls),

        # mobile
        url(r'^shipping/$', views.ShippingView.as_view(template_name='shipping.html'),
            name='shipping'),
        url(r'^shipping_test/$', views.ShippingView.as_view(template_name='shipping_xverify.html'),
            name='shipping'),

        # Your stuff: custom urls includes go here
        url(r'^bottle/', include('oleyrevive.bottles.urls'), name='bottle'),

    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
