# -*- coding: utf-8 -*-
"""
Django settings for oleyrevive project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from __future__ import absolute_import, unicode_literals

import environ

ROOT_DIR = environ.Path(__file__) - 3  # (oleyrevive/config/settings/common.py - 3 = oleyrevive/)
APPS_DIR = ROOT_DIR.path('oleyrevive')

env = environ.Env()
env.read_env()

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',

    # Useful template tags:
    'django.contrib.humanize',

    # Admin
    'django.contrib.admin',
)
THIRD_PARTY_APPS = (
    'crispy_forms',  # Form layouts
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'codemirror2',
    # 'prettyjson',
    'opbeat.contrib.django',
    "djcelery",
    'django_mobile',
    "compressor",

)

# Apps specific for this project go here.
LOCAL_APPS = (
    # custom users app
    'oleyrevive.users.apps.UsersConfig',
    'oleyrevive.main',
    'oleyrevive.limelight',
    'oleyrevive.bottles',
    # Your stuff: custom apps go here
)



# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------

MIDDLEWARE_CLASSES = (
    'opbeat.contrib.django.middleware.OpbeatAPMMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
)

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'oleyrevive.contrib.sites.migrations'
}
HTML_MINIFY = True
EXCLUDE_FROM_MINIFYING = ('^admin/',)

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ("""Derbichev Mikhail""", 'arhangel662@gmail.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///oleyrevive'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates

TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
            str(APPS_DIR.path('static', 'v2')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django_mobile.loader.Loader',
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',

                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django_mobile.context_processors.flavour',
                # 'django_mobile.loader.Loader',
                # Your stuff: custom template context processors go here
            ],
        },
    },
]

# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
URL_FOLDER = ''
version = 'v2'

# STATIC_URL =  "/%s%s" % (URL_FOLDER, STATIC_URL)
FULL_STATIC_URL = '/s/v2/'
MOBILE_STATIC_URL = '/s/v2/mobile/'

# STATIC_URL = '/v2/static/'
STATIC_URL = '/static/'
# STATIC_URL = '/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',

)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# PASSWORD VALIDATION
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'

ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_ADAPTER = 'oleyrevive.users.adapters.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'oleyrevive.users.adapters.SocialAccountAdapter'

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'users:redirect'
LOGIN_URL = 'account_login'

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# Your common stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------


CKEDITOR_CONFIGS = {
    'awesome_ckeditor': {
        'toolbar': 'Basic',
    },
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ],
        'enterMode': 2,
        "removePlugins": "stylesheetparser, htmldataprocessor",
    },

}
# Default config
CODEMIRROR_CONFIG = {
    'lineNumbers': True,
}
LL = {
    'login': 'totalgarcinia',
    'password': 'NgUNHpFyZKpBAz',
    'campaign': '148',
    'campaign_mobile': '182',
}
# LIMELIGHT_API = {
#     'LIMELIGHT_HOST': 'www.oneclickawaytgc.com',
#     'LIMELIGHT_USER': 'totalgarcinia',
#     'LIMELIGHT_PASSWORD': 'NgUNHpFyZKpBAz'
# }

PRICES = {

}

PRODUCTS = {
    'oleyrevive': {'admin': 1, 'shippingId': '15', 'image': 'images/product1.png', 'id': '258', 'descr': '14 Day Trial Package', 'name': 'O’ley Revive Revitalizing Moisturizer', 'price': 0, 'shipping':4.95, 'qty': 1},
    'upsell': {'admin': 1, 'shippingId': '16', 'image': 'images/eyetherapy.png', 'descr': '14 Day Trial Package', 'id': '260', 'name': 'O’ley Revive Ageless Eye Revitalizer', 'price': 0, 'shipping': 4.97, 'qty': 1},
    'upsell2': {'admin': 1, 'image': 'images/serum.png', 'id': '259', 'name': 'O’ley Revive Rejuvenating Facial Serum', 'price': 24.97,'shipping': 0, 'qty': 1},
    'shipinsurance': {'admin': 1, 'id': '276', 'name': 'Shipping Protection', 'descr': 'We will replace any lost or damage packages free of charge', 'image': 'images/protection.png', 'price': 0, 'shipping': 2.99, 'qty': 1},
}


PRODUCTS_FOR_ADMIN = {
    '258': 'Oleyrevive',
    '259': 'Facial Serum',
    '260': 'Eye Revitalizer',
    '276': 'Shipping Protection',
}

PRODUCTS_CHOICES = []
for product in PRODUCTS.values():
    if product.get('admin'):
        PRODUCTS_CHOICES.append((int(product['id']), product['name']))


# GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-89512175-1'
import djcelery
djcelery.setup_loader()
# BROKER_URL = ''
# CELERY_RESULT_BACKEND = ''
# CELERY_ACCEPT_CONTENT = ['pickle']
BROKER_URL = 'redis://localhost:6379/9'
# BROKER_URL = 'redis://localhost:6379/0'
# BROKER_URL = 'django://'
# CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
# BROKER_URL = 'redis://localhost:6379'
# CELERY_RESULT_BACKEND = 'django-db'
# CELERY_RESULT_BACKEND="database"
# CELERY_RESULT_BACKEND = 'django-db'
# CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
# print(329, BROKER_URL)


CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'


#BOTTLES
CYLINDER_PATH = str(APPS_DIR.path('bottles', 'cylinderize.sh'))
BOTTLES_MEDIA_PATH = str(APPS_DIR.path('static', 'v2', 'bottles'))
PREFIX_URL = '/v2'
# PREFIX_URL = ''

COMPRESS_ENABLED = True
COMPRESS_URL = '%s/s/' % PREFIX_URL
CACHE_URL = '/static/CACHE/'

COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter', 'compressor.filters.cssmin.CSSMinFilter']
