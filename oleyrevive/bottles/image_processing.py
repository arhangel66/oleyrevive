# -*- coding: utf8 -*-
import datetime
import os
from math import cos, sin

from django.conf import settings

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'


def calc_coords(im_x, im_y, angle, items_count=3, size_low=0.9):
    ran = 0.5
    # calc width for new image
    otst = list(
        int(ran * im_x * size_low ** (i // 2) * cos(angle * 3.14159 / 180)) for i in list(range(items_count + 1))[::2]
        if i > 0)
    new_width = int(sum(otst) * 2 + im_x * size_low ** (items_count // 2))

    # calc coords for images
    coords = []
    otst = 0
    for i in list(range(items_count + 1))[::2]:
        mult = (size_low ** (i // 2))

        if i > 0:
            otst += int(im_x * mult * ran)
        # print(i, otst)
        x = int(new_width / 2 - im_x / 2 + cos(angle * 3.14159 / 180) * otst)
        y = int(im_y - int(sin(angle * 3.14159 / 180) * otst) - im_y * mult)
        coords.append((x, y))
        if i != 0 and i < items_count:
            x = int(new_width / 2 - im_x / 2 - cos(angle * 3.14159 / 180) * otst) + int(im_x - im_x * mult)
            coords.append((x, y))
    return coords, new_width


def make_miltiple_items(image_path='bottle.png', items_count=7, images=[]):
    # open image
    from PIL import Image
    angle = 15
    size_low = 0.8
    im = Image.open(image_path)
    im_x = im.size[0]
    im_y = im.size[1]

    coords, new_width = calc_coords(im_x, im_y, angle, items_count)
    result = Image.new("RGBA", (new_width, im_y))

    for item in list(range(items_count))[::-1]:
        mult = (size_low ** ((item + 1) // 2))
        img2 = im.resize((int(mult * im_x), int(mult * im_y)))
        result.paste(img2, (coords[item][0], coords[item][1]), img2)

    # save result
    result.save("result/bottle_%s.png" % items_count)


def make_miltiple_items_from_images(images=[]):
    # open image
    from PIL import Image
    directory = settings.BOTTLES_MEDIA_PATH
    size_low = 0.9
    angle = 15
    im = Image.open(images[0])
    items_count = len(images)
    im_x = im.size[0]
    im_y = im.size[1]

    coords, new_width = calc_coords(im_x, im_y, angle, items_count, size_low)
    result = Image.new("RGBA", (new_width, im_y))

    for item in list(range(items_count))[::-1]:
        im = Image.open(images[item])
        mult = (size_low ** ((item + 1) // 2))
        img2 = im.resize((int(mult * im_x), int(mult * im_y)))
        result.paste(img2, (coords[item][0], coords[item][1]), img2)

    # save result
    dt = datetime.datetime.now().strftime("%M-%S")
    file_name = "bottle_%s_%s.png" % (items_count, dt)
    full_path = os.path.abspath(os.path.join(directory, file_name))
    result.save(full_path)
    return {'name': file_name, 'path': full_path}

# for now it is working only with transparent images. if you need jpg - just tell me.
# make_miltiple_items_from_images(images=['label7.png', 'label8.png', 'label9.png'])
# coords, new_width = calc_coords(100, 200, 15)
# print(coords, new_width)
# make_miltiple_items('bottle.png', 2)
# make_miltiple_items('label.png', 3)
# make_miltiple_items('bottle.png', 4)
# make_miltiple_items('label7.png', 5)
# make_miltiple_items('bottle.png', 7)

# print('done, check folder result')
