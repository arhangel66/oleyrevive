# -*- coding: utf8 -*-
import datetime
from subprocess import call
from time import sleep
from zipfile import ZipFile

from oleyrevive.bottles.image_processing import make_miltiple_items_from_images

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'

import os

from django.core.files.storage import FileSystemStorage

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from crispy_forms.bootstrap import StrictButton
from django import forms
from django.conf import settings


def handle_uploaded_file(file):
    directory = settings.BOTTLES_MEDIA_PATH
    storage = FileSystemStorage(location=directory)

    # Check to see if Users folder already exists
    if not storage.exists(directory):
        os.makedirs(directory)
        os.chmod(directory, 777)

    # Use djangos FileSystemStorage to see if file exists will rename accordingly
    # if storage.exists(file.name):
    #     file.name = storage.get_available_name(file.name)

    with open(os.path.abspath(os.path.join(directory, file.name)), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return os.path.abspath(os.path.join(directory, file.name))


class BottleForm(forms.Form):
    BOTTLE_TYPES = (
        ('white', 'white'),
        ('black', 'black'),
        ('dropper', 'dropper')
    )
    label = forms.ImageField(label='Label', help_text='jpg or png file with label', required=True)
    bottle_type = forms.ChoiceField(required=True, choices=BOTTLE_TYPES)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-10'
        self.helper.layout = Layout(
            'label',
            'bottle_type',
            StrictButton('Submit', css_class='btn-default', type='submit')
        )

    def start_command(self, label_path, bottle_type):
        dt = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        files = [
            {'name': 'side_main_{}.png'.format(dt), 'angle': 0, 'w': 90},
            {'name': 'side_left_{}.png'.format(dt), 'angle': 140, 'w': 100},
            {'name': 'side_right_{}.png'.format(dt), 'angle': -140, 'w': 100},
         ]

        bottle_types = {
            'white': {
                'bottle': 'mybottle2.png',
                'params': "-m vertical -r 175 -l 417 -w {w} -p 5 -n 104 -d both -e 1 -a {angle} -v background -b none -f none -o +3+73"
            },
            'black': {
                'bottle': 'black_bottle.png',
                'params': "-m vertical -r 201 -l 465 -w {w} -p 12 -n 104 -d both -e 1 -a {angle} -v background -b none -f none -o -1+53"
            },
            'dropper': {
                'bottle': 'dropper.png',
                'params': "-m vertical -r 109 -l 290 -w {w} -p 10 -n 100 -e 1 -a {angle} -v background -b none -f none -o +2+112"
            },
        }
        bottle_dict = bottle_types.get(bottle_type)
        res = {}
        res['commands'] = []
        res['files'] = []



        for fil in files:
            params = {
                'cyl_path': settings.CYLINDER_PATH,  #"/home/django/projects/oleyrevive/oleyrevive/static/v2/t/cylinderize.sh",
                'command_params': bottle_dict['params'].format(**fil),
                'back_path': "/home/django/projects/oleyrevive/oleyrevive/static/v2/t/{}".format(bottle_dict['bottle']),
                'result_path': os.path.join(settings.BOTTLES_MEDIA_PATH, fil['name']),
                'label_path': label_path
            }

            command = "bash {cyl_path} {command_params} {label_path} {back_path} {result_path}".format(**params)
            call(command, shell=True)
            res['commands'].append(command)
            res['files'].append({'path': params.get('result_path'), 'url': '/s/v2/bottles/{}'.format(fil['name']), 'name': fil['name']})
        return res

    def save(self):
        cd = self.cleaned_data
        label_path = handle_uploaded_file(cd['label'])
        res = self.start_command(label_path, cd['bottle_type'])
        images = list(file['path'] for file in res['files'])
        res['comp'] = make_miltiple_items_from_images(images)

        images = []
        res['basic_comp'] = []
        for i in range(6):
            images.append(res['files'][0]['path'])
            res['basic_comp'].append(make_miltiple_items_from_images(images))
            sleep(1)

        #zip
        with ZipFile(os.path.join(settings.BOTTLES_MEDIA_PATH, 'result.zip'), 'w') as myzip:
            for file in res['files']:
                myzip.write(file['path'], file['name'])
            myzip.write(res['comp']['path'], res['comp']['name'])
            for fil in res['basic_comp']:
                myzip.write(fil['path'], fil['name'])
            res['zip_url'] = '/s/v2/bottles/result.zip'

        # print(32, cd)
        return res
