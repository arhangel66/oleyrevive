# -*- coding: utf8 -*-
__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.BottleFormView.as_view(), name='bottle-form'),
]
