# -*- coding: utf8 -*-
import os

from braces.views import StaffuserRequiredMixin
from django.views.generic import FormView
from . import forms

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'


class BottleFormView(FormView):
    form_class = forms.BottleForm
    template_name = 'bottle_form.html'

    def form_valid(self, form):
        result = form.save()
        # self.form = self.get_form(self.get_form_class())
        # print()
        # self.kwargs['form']
        # self.form = None
        return self.render_to_response(self.get_context_data(form=form, result=result))



