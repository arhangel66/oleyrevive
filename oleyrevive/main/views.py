import re

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import AccessMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import Template, RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView

from oleyrevive.limelight.api import LimeLightApi, get_client_ip
from oleyrevive.limelight.card_type import get_card_type
from oleyrevive.limelight.models import CrmLogs, Order
from oleyrevive.limelight.tasks import call
from oleyrevive.main.cart import Cart
from oleyrevive.main.models import SnippetGlobal, SnippetPage, SnippetProduct

oleyrevive_id = 235


class HasOrderMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        # print(30, request.session.get('main_order_id'), request.session.get('order_task'))
        if not (self.request.GET.get('is_test') or request.session.get('main_order_id') or request.session.get(
                'order_task') or request.session.get('testmobile')):
            return HttpResponseRedirect(self.get_back())

        return super().dispatch(request, *args, **kwargs)


class HasProspectMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.GET.get('is_test') and not request.session.get('prospect_task'):
            return HttpResponseRedirect(self.get_back())
        return super().dispatch(request, *args, **kwargs)


class AbstractBaseView(object):
    def is_celery_error(self, celery_task_id, request=None):
        if request.session.get(celery_task_id) is None:
            return False
        log_id = call.AsyncResult(request.session[celery_task_id]).get()
        return is_log_error(log_id, request)


def is_log_error(log_id, request):
    log = CrmLogs.objects.get(id=log_id)
    if log.is_error:
        # messages.add_message(request, messages.INFO, log.get_error_text())
        if request:
            messages.add_message(request, messages.INFO, log.get_error_text())
        return True
    return log


class AbstractMixin(object):
    next = '/'
    back = '/'
    this = '/'
    # prefix = '/v2'
    prefix = settings.PREFIX_URL

    def apply_prefix(self, url):
        return "%s%s" % (self.prefix, url)

    def get_next(self):
        if '/' in self.next:
            next = self.next
        else:
            try:
                next = reverse(self.next)
            except:
                next = self.next
        return self.apply_prefix(next)

    def get_back(self):
        if '/' in self.back:
            back = self.back
        else:
            try:
                back = reverse(self.back)
            except:
                back = self.back
        return self.apply_prefix(back)

    def get_this_url(self):
        if '/' in self.this:
            this = self.this
        else:
            try:
                this = reverse(self.this)
            except:
                this = self.this
        # print(105, this, self.apply_prefix(this))
        return self.apply_prefix(this)


class MyTemplateView(AbstractMixin, TemplateView, AbstractBaseView):
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['request'] = request

        context['init'] = request.session.get('user_data', {})
        for key in ['CVV', 'csrfmiddlewaretoken']:
            if key in context['init']:
                context['init'].pop(key)
        # print(117, request.session.get('descriptors'))
        # print(116, request.GET.get('testmobile'), request.session.get('testmobile'))
        # if not (request.GET.get('testmobile') or request.session.get('testmobile')):
        #     set_flavour('full')
        # else:
        #     set_flavour('mobile')

        context.update(self.request.session.setdefault('for_template', {}))
        # messages.add_message(request, messages.INFO, 'test41')
        html = render_to_string(self.get_template_names()[0], context,
                                context_instance=RequestContext(request))

        just_bought_products = request.session.pop('just_bought') if request.session.get('just_bought') else None
        html = self.process_snippets(html, page=request.path, just_bought_products=just_bought_products)


        html = self.change_static(html, flavour=request.flavour)
        html = self.set_csrf_token(html)
        html = self.add_compress(html)
        # print(23, html)

        t = Template(html)

        html = t.render(RequestContext(request, context))
        html = html.replace('/s/CACHE/', settings.CACHE_URL)
        # print(27, html)
        return HttpResponse(html)

    # def change_static(self, html):
    #     html = re.sub(r'(src="|href=")([^"]+\.html"|js|img|css|images|fonts)', '\\1%s/s/v2/\\2' % settings.PREFIX_URL,
    #                   html)
    #     return html

    def change_static(self, html, flavour=None):
        path = settings.PREFIX_URL + settings.FULL_STATIC_URL

        if flavour == 'mobile':
            path = settings.PREFIX_URL + settings.MOBILE_STATIC_URL

        html = re.sub(r'(src="|href=")([^"]+\.html"|js|img|i|css|images|fonts)', '\\1%s\\2' % path, html)
        html = re.sub(r'(openNewWindow\(\&\#39\;)([^&]+\.html\&\#39\;)', '\\1%s\\2' % path, html)
        html = re.sub(r'(<input[^>]+data-qty=\")([^"]+)(\")', '\\1\\2" value="\\2\\3', html)  # add val to select inputs
        html = html.replace('//static', '/static')
        return html

    def set_csrf_token(self, html):
        html = re.sub(r'(<form[^>]+>)', '\\1 \n {% csrf_token %}', html)
        return html

    def process_snippets(self, html, page, just_bought_products=None):
        """
        Add all snippets to html page.
        Returns new updated html page.
        """
        dic = {}
        # global
        for s in SnippetGlobal.objects.filter(is_active=True):
            dic[s.name] = s

        # page
        for s in SnippetPage.objects.filter(page=page, is_active=True):
            dic[s.name] = s

        # product
        if just_bought_products:
            for s in SnippetProduct.objects.filter(product__in=just_bought_products, is_active=True):
                dic[s.name] = s

        for name, s in dic.items():
            html = s.add_snippet(html)

        return html

    def add_compress(self, html):
        if '<!-- my_css -->' in html:
            html = "{% load compress %}" + html
            html = html.replace('<!-- my_css -->', """{% compress css %}""")
            html = html.replace('<!-- my_end_css -->', """{% endcompress %}""")
            html = html.replace('<!-- my_js -->', """{% compress js %}""")
            html = html.replace('<!-- my_end_js -->', """{% endcompress %}""")
        return html

class SendProspectMixin(AbstractMixin):
    def post(self, request, *args, **kwargs):
        prospect_data = request.POST.dict()
        request.session.setdefault('user_data', {})
        request.session['user_data'].update(prospect_data)
        # return HttpResponseRedirect('/')

        api = LimeLightApi()
        prospect_data['ipAddress'] = get_client_ip(request)
        prospect_data['address1'] = prospect_data.get('shippingAddress1') if 'shippingAddress1' in prospect_data else ''
        prospect_data['country'] = prospect_data.get('shippingCountry') if 'shippingCountry' in prospect_data else ''
        prospect_data['state'] = prospect_data.get('shippingState') if 'shippingState' in prospect_data else ''
        prospect_data['city'] = prospect_data.get('shippingCity') if 'shippingCity' in prospect_data else ''
        prospect_data['zip'] = prospect_data.get('shippingZip') if 'shippingZip' in prospect_data else ''
        prospect_data = api.add_spec_params(request, prospect_data)
        request.session['prospect_task'] = api.send_prospect(prospect_data).id
        request.session['for_template'].update(prospect_data)
        # create prospect in DB
        # prospect_dict_for_db = Prospect().prepare_dict_from_ll(prospect_data)
        # prospect_dict_for_db['task_id'] = request.session['prospect_task']
        # prospect = Prospect.objects.create(**prospect_dict_for_db)

        return HttpResponseRedirect(self.get_next())


class SendOrderMixin(AbstractMixin):
    product = ''
    next = '/'

    def post(self, request, *args, **kwargs):
        log = self.is_celery_error('prospect_task', request)
        if log is True:
            return HttpResponseRedirect(self.get_back())
        log.save_prospect()

        prospect_id = log.get_prospect_id()
        request.session['prospect_id'] = prospect_id

        # Prospect.objects.filter(task_id=request.session['prospect_task']).update(limelight_id=prospect_id)
        order_data = request.POST.dict()
        order_data['billingSameAsShipping'] = 'YES' if 'billingSameAsShipping' in order_data else 'NO'

        request.session.setdefault('user_data', {})
        request.session['user_data'].update(order_data)

        # return HttpResponseRedirect(self.get_next())
        order_data['expirationDate'] = "%s%s" % (order_data.get('expmonth'), order_data.get('expyear'))
        order_data['tranType'] = 'Sale'
        order_data['shippingCountry'] = 'US'

        if not order_data.get('creditCardType'):
            order_data['creditCardType'] = get_card_type(order_data.get('creditCardNumber', None))

        if 'cc_num' in order_data:
            order_data['creditCardNumber'] = order_data.pop('cc_num')

        product = settings.PRODUCTS.get(self.product)
        order_data = self.add_product_to_order_data(order_data, product)

        api = LimeLightApi()

        # request.session['order_task'] = api.send_order_with_prospect(order_data, prospect_id).id
        order_data = api.add_spec_params(request, order_data)
        log_id = api.send_order_with_prospect(order_data, prospect_id, async=False)
        log = is_log_error(log_id, request)
        if log is True:
            return HttpResponseRedirect(self.get_this_url())
        request.session['main_order_id'] = log.get_order_id()
        request.session['descriptors'] = [log.get_descriptor()]

        cart = Cart(request)
        cart.clear()
        cart.add_to_cart(product)
        request.session['tasks'] = []
        # insurance
        if not 'shipinsurance' in order_data:
            product = settings.PRODUCTS.get('shipinsurance')
            # order_data = self.add_product_to_order_data(order_data, product)
            # request.session['insurance_task'] = api.send_order_with_prospect(order_data, prospect_id).id
            request.session['tasks'].append(api.new_order_card_on_file_async(
                request.session['main_order_id'], product['id'], price=product['price'],
                prospect_id=request.session.get('prospect_id'), product=product, request=request))
            cart.add_to_cart(product)

        return HttpResponseRedirect(self.get_next())

    def add_product_to_order_data(self, order_data, product):
        if product and 'id' in product:
            order_data['productId'] = product['id']
            if 'price' in product and product['price'] > 0:
                order_data['dynamic_product_price_' + str(product['id'])] = product['price']
            if 'qty' in product and product['qty'] != 1:
                order_data['product_qty_%s' % product['id']] = product['qty']
            if 'shippingId' in product:
                order_data['shippingId'] = product['shippingId']
        return order_data


class SendUpsellMixin(AbstractMixin):
    actions = {}
    next = '/'

    def get(self, request, *args, **kwargs):
        key = request.GET.get('action')
        if key:
            product_key = self.actions.get(key)
            if product_key is None:
                return HttpResponseRedirect(self.get_next())
            product = settings.PRODUCTS.get(product_key)

            # messages.add_message(request, messages.INFO, 'test132')
            # log = self.is_celery_error('order_task', request)
            # if log is True:
            #     return HttpResponseRedirect(self.get_back())

            # request.session['main_order_id'] = log.get_order_id()

            api = LimeLightApi()
            request.session['tasks'].append(api.new_order_card_on_file_async(
                request.session['main_order_id'], product['id'], price=product['price'],
                prospect_id=request.session.get('prospect_id'), product=product, request=request))
            cart = Cart(request)
            cart.add_to_cart(product)

            return HttpResponseRedirect(self.get_next())
        return super().get(request, *args, **kwargs)


class HomeView(MyTemplateView, SendProspectMixin):
    next = '/checkout/'

    def get(self, request, *args, **kwargs):
        user_data = request.session.get('user_data', {})
        request.session.delete()
        if request.method == 'GET' and 'AFID' in request.GET:
            request.session['AFID'] = request.GET['AFID']
        else:
            request.session['AFID'] = 'oleyrevive.com'  # request.get_host()

        if request.method == 'GET' and 'SID' in request.GET:
            request.session['SID'] = request.GET['SID']

        if request.method == 'GET' and 'AFFID' in request.GET:
            request.session['AFFID'] = request.GET['AFFID']

        if 'custom' in request.GET:
            request.session['OPT'] = request.GET['custom']

        if 'testmobile' in request.GET:
            request.session['testmobile'] = 1

        request.session.save()
        request.session['user_data'] = user_data

        # print(301, request.flavour, self.get_template_names())
        return super().get(request, *args, **kwargs)


class CheckoutView(HasProspectMixin, MyTemplateView, SendOrderMixin):
    next = '/upsell/'
    product = 'oleyrevive'
    this = 'checkout'
    back = 'home'


class UpsellView(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/upsell2/'
    actions = {'yes': 'upsell'}
    back = 'checkout'


class Upsell2View(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/success/'
    actions = {'yes': 'upsell2'}
    back = 'checkout'


class SuccessView(HasOrderMixin, MyTemplateView):
    back = 'checkout'

    def clean_session(self):
        list_keys = ['main_order_id', 'prospect_task', 'order_task']
        for key in list_keys:
            if key in self.request.session:
                self.request.session.pop(key)

    def get_descriptors(self):
        descriptors = self.request.session.get('descriptors', [])

        for task_id in self.request.session.get('tasks', []):
            log_id = call.AsyncResult(task_id).get()
            log = CrmLogs.objects.get(id=log_id)
            if log.is_error:
                continue
            else:
                descriptors.append(log.get_descriptor())
        return list(set(descriptors))

    def get(self, request, *args, **kwargs):
        log = self.is_celery_error('order_task', request)
        # if log is True:
        #     return HttpResponseRedirect(self.get_back())
        # print(201)
        # if not request.session.get('main_order_id'):
        #     print(202, log, log.get_order_id())
        #     request.session['main_order_id'] = log.get_order_id()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart = Cart(self.request)
        order_id = self.request.session.get('main_order_id',
                                            Order.objects.filter(limelight_id__isnull=False).last().limelight_id)
        if len(cart.products) == 0 and (self.request.user.is_staff or self.request.session.get('testmobile')):
            cart.add_to_cart(settings.PRODUCTS['oleyrevive'])
            cart.add_to_cart(settings.PRODUCTS['shipinsurance'])
            cart.add_to_cart(settings.PRODUCTS['upsell'])
            cart.add_to_cart(settings.PRODUCTS['upsell2'])
            order_id = Order.objects.all().last().limelight_id
        context['cart'] = cart
        context['order_id'] = order_id
        context['descriptors'] = self.get_descriptors()
        # if 'main_product_id' in self.request.session:
        #     context['main_product'] = settings.PRODUCTS.get(self.request.session.get('main_product_id'))
        order = Order.objects.filter(limelight_id=order_id).last()
        context['address'] = order.get_address_dict()
        context['order_data'] = order.get_order_data()
        # self.clean_session()
        return context

    def get_total(self, order):
        total = 0
        for line in order:
            if line and type(line) == type({}) and line.get('total'):
                total += float(line.get('total'))
        return round(total, 2)


class OnlyMobileMixin(AccessMixin):
    """
    if it is not mobile will redirect to back page
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if request.flavour != 'mobile':
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)

# OnlyMobileMixin,
class ShippingView(MyTemplateView, SendProspectMixin):
    back = 'home'
    next = 'checkout'