# -*- coding: utf8 -*-
from config.settings.common import PRODUCTS_CHOICES

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
from django.db import models
from django.contrib import admin
from .snippet_base import SnippetBase


class SnippetGlobalManager(models.Manager):
    def create_snippet(self, name, value, type):
        s = self.model(name=name, value=value, type=type)
        s.save()
        return s



class SnippetProduct(SnippetBase):
    objects = SnippetGlobalManager()
    product = models.IntegerField(choices=PRODUCTS_CHOICES)

    class Meta:
        verbose_name = 'snippet: product'
        verbose_name_plural = 'snippets: product'
        ordering = ['name']

    def __str__(self):
        return self.name


@admin.register(SnippetProduct)
class SnippetProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'product', 'type', 'value', 'is_active')
    list_filter = ('product', )
    list_editable = ('value', 'type', 'is_active')
