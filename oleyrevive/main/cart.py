# -*- coding: utf8 -*-
__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'


class Cart(object):
    def __init__(self, request=None):
        self.request = request
        self.products = request.session.setdefault('order', []) if request else []
        self.total = ''

    def add_to_cart(self, product):
        product['total'] = product.get('price', 0) * product.get('qty', 0) + product.get('shipping', 0)
        product['total_price'] = product.get('price', 0) * product.get('qty', 0)
        self.products.append(product)
        if self.request:
            self.request.session['order'] = self.products
            self.just_bought(product['id'])

    def just_bought(self, product_id):
        if not 'just_bought' in self.request.session:
            self.request.session['just_bought'] = []
        self.request.session['just_bought'].append(product_id)
        print(24, self.request.session['just_bought'])

    def clear(self):
        self.products = []
        print('cart cleaned')

    def calc_total(self):
        return sum(list(product.get('total', 0) for product in self.products))

    def calc_total_price(self):
        return sum(list(product.get('total_price', 0) for product in self.products))

    def calc_total_shipping(self):
        return sum(list(product.get('shipping', 0) for product in self.products))
