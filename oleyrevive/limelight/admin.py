from django import forms
from django.contrib import admin
# from prettyjson import PrettyJSONWidget
from oleyrevive.limelight.models import CrmLogs, Order, Prospect


# class JsonForm(forms.ModelForm):
#     class Meta:
#         model = CrmLogs
#         fields = '__all__'
#         widgets = {
#             'get_text': PrettyJSONWidget(),
#             'send_text': PrettyJSONWidget(),
#         }


@admin.register(CrmLogs)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'command_type', 'is_error', 'created', 'get_time')
    list_filter = ('url', 'is_error', 'command_type',)

    # form = JsonForm


@admin.register(Order)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'product_id', 'product_name', 'count', 'order_total', 'is_real', 'get_ll_link', 'created_time')
    list_filter = ('is_real',)

    # form = JsonForm
@admin.register(Prospect)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'phone', )

    # form = JsonForm
